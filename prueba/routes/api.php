<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/* lista empleados */
Route::get("/empleados","App\Http\Controllers\EmployeeController@index");
/* cargos por id */
Route::get("/empleados/{id}","App\Http\Controllers\EmployeeController@ById");
/* crear empleado */
Route::post("/crear_empleados","App\Http\Controllers\EmployeeController@create");
/* editar empleado */
Route::post("/editar_empleados","App\Http\Controllers\EmployeeController@edit");
/* eliminar empleado */
Route::post("/eliminar _empleados","App\Http\Controllers\EmployeeController@destroy");


/* lista de cargos */
Route::get("/cargos","App\Http\Controllers\EmployeeSalaryController@index");
/* cargos por id */
Route::get("/cargos/{id}","App\Http\Controllers\EmployeeSalaryController@ById");
/* crear cargo */
Route::post("/crear_cargo","App\Http\Controllers\EmployeeSalaryController@create");
/* editar cargo */
Route::post("/editar_cargo","App\Http\Controllers\EmployeeSalaryController@edit");
/* eliminar cargo */
Route::post("/eliminar_cargo","App\Http\Controllers\EmployeeSalaryController@destroy");
