<?php

namespace App\Http\Controllers;

use App\Models\employee_salary;
use Illuminate\Http\Request;

class EmployeeSalaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cargos = employee_salary::all();

        return $cargos;
    }

    public function ById(Request $request){
        $cargos = employee_salary::find($request->id);
        return $cargos;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        
        $employee_salary = new employee_salary;
    
        $employee_salary->salario = $request->salario;
        $employee_salary->impuestos = $request->salario*0.08;
        $employee_salary->valor_prima = $request->salario*2;
        $employee_salary->salud = ($request->salario*2)*0.08;
        $employee_salary->pension = $request->salario*0.2;
        $employee_salary->cargo = $request->cargo;
       
       
        if($employee_salary->save()){
            return 'Cargo Creado';
        }else{
            return 'Cargo No Creado';
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\employee_salary  $employee_salary
     * @return \Illuminate\Http\Response
     */
    public function show(employee_salary $employee_salary)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\employee_salary  $employee_salary
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $id = $request->id;
        
        $employee_salary = employee_salary::findOrFail($id);
        
        $employee_salary->salario = $request->salario;
        $employee_salary->impuestos = $request->salario*0.08;
        $employee_salary->valor_prima = $request->salario*2;
        $employee_salary->salud = ($request->salario*2)*0.08;
        $employee_salary->pension = $request->salario*0.2;
        $employee_salary->cargo = $request->cargo;
       
        if($employee_salary->save()){
            return 'Cargo editado';
        }else{
            return 'Cargo No editado';
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\employee_salary  $employee_salary
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, employee_salary $employee_salary)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\employee_salary  $employee_salary
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->id;
        
        $employee_salary = employee_salary::findOrFail($id);
        
        if($employee_salary->delete()){
            return 'Cargo eliminado';
        }else{
            return 'Cargo No eliminado';
        }
    }
}
