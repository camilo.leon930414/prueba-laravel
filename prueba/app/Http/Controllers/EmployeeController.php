<?php

namespace App\Http\Controllers;

use App\Models\employee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $empleados = employee::all();

        return $empleados;
    }

    public function ById(Request $request){
        $cargos = employee::find($request->id);
        return $cargos;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        
        $employee = new employee;
        
        $employee->nombre = $request->nombre;
        $employee->documento = $request->documento;
        $employee->telefono = $request->telefono;
        $employee->direccion = $request->direccion;
        $employee->cargo = $request->cargo;
       
       
        if($employee->save()){
            return 'Empleado Creado';
        }else{
            return 'Empleado No Creado';
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $id = $request->id;
        
        $employee = employee::findOrFail($id);
        
        $employee->nombre = $request->nombre;
        $employee->documento = $request->documento;
        $employee->telefono = $request->telefono;
        $employee->direccion = $request->direccion;
        $employee->cargo = $request->cargo;
       
        if($employee->save()){
            return 'Empleado editado';
        }else{
            return 'Empleado No editado';
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, employee $employee)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->id;
        
        $employee = Employee::findOrFail($id);
        
        if($employee->delete()){
            return 'Empleado eliminado';
        }else{
            return 'Empleado No eliminado';
        }
    }
}
