<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class employee extends Model
{
    protected $table = "employees";
    # No queremos que ponga updated_at ni created_at
    public $timestamps = false;
}
